# General Info

Sets up two project:
* https://gitlab.com/tjnikkila/music-generation-front.git
* https://gitlab.com/tjnikkila/music-generation-api.git

# To Build:

```sh 
git clone https://gitlab.com/tjnikkila/music-generation-docker.git
cd music-generation-docker
docker build --tag=midimusic .
```

# To Run

```
docker run -d -p8080:8081 -p 5000:5001 midimusic
```

# How to test

Open http://localhost:8080/ in your browser (tested by Chrome).

# How to stop

* Check current CONTAINER ID `docker ps`
* Stop process `docker stop <your CONTAINER ID>`
* You may want to clean all containers and images after you are done. 


# Problems
## One sound does not stop 
Some sound can get stuck when running demo. Fix is to refresh browser.

## One key stroke gives two sounds
Fix is to refresh browser.