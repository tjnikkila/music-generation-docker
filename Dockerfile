FROM python:3.7

WORKDIR /app

COPY runapp.sh /app

RUN curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.deb.sh | bash && \
    apt-get install git-lfs && \
    git lfs install && \
    git clone https://gitlab.com/tjnikkila/music-generation-api.git && \
    cd music-generation-api && \
    pip install -r requirements.txt && \
    pip install torch==1.2.0+cpu torchvision==0.4.0+cpu -f https://download.pytorch.org/whl/torch_stable.html

RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - && \
    echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list && \
    apt-get update && apt-get install -y yarn && \
    git clone https://gitlab.com/tjnikkila/music-generation-front.git && \
    cd music-generation-front && \
    yarn

EXPOSE 5001
EXPOSE 8081

ENV NAME musicapp

ENTRYPOINT ["/app/runapp.sh"]
