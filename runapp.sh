#!/bin/bash

FLASK_APP=music-generation-api/backend.py flask run  --host=0.0.0.0 --port=5001 &

cd music-generation-front
yarn run serve  --port 8081
